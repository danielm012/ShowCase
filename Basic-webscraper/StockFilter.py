"Created by Daniel Mejia, 7/25/2016"
"All rights reserve only to the creator, only the creator has right to copy, edit or distribute this code!"
"This code is free to be used by anyone else for any purpose, except for the modification and distribution of the code"
"Stock analysis software, alert, recommedation and more"

import requests, bs4, re, sys, time
global globalavg

pricehistory = []
# positive or negative attributes
def checking(variable):
        positive = 0; negative = 0; stable = 0
        if(variable > 0):
            positive = positive + 1
        elif(variable < 0):
            negative = negative + 1
        else:
            stable = stable + 1
        return positive, negative, stable

def pricefinder(price, maximum, minimum, openprice):
        if (type(price) != float):
                price = float(price)
                maximum = float(maximum)
                minimum = float(minimum)
                openprice = float(openprice)
        #print(price, maximum,minimum,openprice)
        avgval = maximum + minimum
        avgval = avgval / 2.00; #print(avgval)
        avgmax = maximum + openprice
        avgmin = minimum + openprice
        avgmax = avgmax / 2.00; 
        avgmin = avgmin / 2.00; 
        avg = avgmin / avgmax; #print('percent: ',avg)
        avg = 1 - avg
        pricevallow = price - price*avg; #print(priceval)
        pricevalhigh = price + price*avg
        return pricevallow, avgval, pricevalhigh
        

# find the average
def average(variable):
    total = 0.00 
    for i in range(len(variable)):
        temp = variable[i]
        final = float(temp)
        total = total + final
        average = total/len(variable)
    return average
    
# Class which contains variables for stock analysis
class Stocks:
    Name = [];    Price = [];    Daymax = 0;    Daymin = 0
    Yearmax = 0;    Yearmin = 0;     openprice = 0;      Vmax = 0;    vmin = 0;    Divident = 0; Eps = 0;        Shares = 0
    Beta = 0;     Temp = [];   History = []
    
# function removes symbols from numbers to be use in calculations& valid variables
def selectvalue(a, b):
    if(b == 3):
            if(a != None):
                    separation = re.compile(r'(\d*.\d*\w)')
                    value = separation.search(a);
                    var = value.group(1)
                    return var
            else:
                    return 0
    if(b == 2):
        separation = re.compile(r'(\d+\.\d+)...(\d+\.\d+)')
        value = separation.search(a); #print(value)
        if (value == None):
            Min = '0'; #print(Min)
            Max = '0'; #print(Max)
            return Max, Min
        else:
            Min = value.group(1); #print(Min)
            Max = value.group(2); #print(Max)
            return Max, Min
            
    if(b == 1):
        one = re.compile(r'(\d.+)')
        single = one.search(a)
        if (single == None):
            return '0'
        else:
            temp = single.group(1); #print(temp)
            return temp
    if(b == 0):
        #one = re.compile(r'([-+]?\d+.\d+)'); # filter values with symbol + or - limited to special use
        one = re.compile(r'^[-+]?(\d+.\d+)$|(\d+.\d+)|(\d*\w)')
        value = one.search(a)
        var = value
        #print('var: ', var)
        if(var == None):
            return '0'
        else:
            var = value.group(0)
            return var
    else:
        print("\n Error range out of norm!")

# Check variables inside stocks for aproval of function execution
def check(variable):
    if (variable == None):
        return 0
    else:
        return variable

#Function that gives recommendation base on factors from Function alert
def theory(data):
        positive = data[0]; negative = data[1]; avgslope = data[2]; slope = data[3]
        if (positive >= 6 and negative <= 2 and avgslope > 0):
                print(' Extreme increased!\n', "recommendation: strong buy to sell at high price or hold to sell,  Risk low")
        elif (positive >= 5 and negative <= 2 and avgslope >= 0):
                print(' Extremely stable!\n recommendation: buy or sell at high price, Risk low')
        elif (positive >= 5 and negative == 3):
                print(' Extremely stable!\n recommendation: buy or sell at high price, Risk low-medium')
        elif (positive == 4 and negative <= 2):
                print (' Very stable!\n recommendation: positive buy, sell, or wait for more data!, Risk medium')
        elif (positive >= 3 and positive < 5 and negative >= 3):
                print (' Stable\n recommendation: hold for more data!')
        elif (positive == 3 and negative < 3):
                print(' Stable\n recommendation: hold, buy or sell, or wait for more data, Risk medium')
        elif (positive == 2 and negative >= 2 and avgslope <= 0 or positive == 3 and negative > 2):
                print(' Unstable!\n recommendation: hold, Dont buy, Risk medium-high')
        elif (positive == 1 and negative > 2 and avgslope < 0 or positive == 1 and avgslope >= 0):
                print(' Very unstable!\n recommendation: high risk buy, limit loss sell, Risk high')
        elif (positive == 0 and negative >= 1):
                print(' Abort\n', 'recommendation: Stop loss sell or take the risk plunge!, Risk extreme')
        elif (positive == 0):
                print(' Not enough data, N/A')
        else:
                print('No define action for positive: ',positive, 'negative: ',negative, 'slope: ',slope)
                return positive, negative, slope
        
    
#Main function for calculating values and data analysis!
def alert(price, variable, openprice, daymax, daymin, yearmin, yearmax, eps, beta):
    global globalavg
    price = float(price); openprice = float(openprice); daymax = float(daymax); daymin = float(daymin); yearmin = float(yearmin); yearmax = float(yearmax); eps = float(eps); beta = float(beta)
    positive = 0; negative = 0; total = 0; stable = 0; averageslope=0; totalslope = 0
    globalavg = average(variable); #print('glovalavg: ',globalavg, type(globalavg))
    p = price
    flowslope = price - openprice
    avgslope = p - globalavg; #print('avgslope: ', avgslope);
    globalavg = str(globalavg)
    percentyearmin = yearmin + yearmin * 0.20
    percentyearmax = yearmax - yearmax * 0.10
    percentdaymax = daymax - daymax * 0.10;
    percentdaymin = daymin + daymin * 0.20
    pricetarget = pricefinder(price, daymax, daymin, openprice)
    pricedaytarget = pricetarget[0]
    if(len(variable) > 1):
            for i in range(len(variable)-1):
                totalslope = float(variable[i+1]) - float(variable[i])
                averageslope = totalslope + averageslope; #print('Slope: ', averageslope )
    else:
        averageslope = 0
        
    if(avgslope >= 0):
        if(price <= pricedaytarget):
            positive = positive + 1
        else:
            negative = negative + 1
        if(flowslope >= 0):
            positive = positive + 1
        else:
            negative = negative + 1
        if(averageslope >= 0.20):
            positive = positive + 2
        else:
            positive = positive + 1
        if(p > percentdaymax):
            positive = positive + 1
        else:
            negative = negative + 1
        if(p >= percentyearmax):
            negative = negative + 1
        else:
            positive = positive + 1
        if(p >= percentyearmin):
            positive = positive + 1
        if(eps > 1.50):
            positive = positive + 1
        else:
            negative = negative + 1
        if(beta > 1):
            negative = negative + 1
        else:
            positive = positive + 1
    else:
        if(price <= pricedaytarget):
            postive = positive + 1
        if(flowslope >= 0):
            positive = positive + 1
        else:
            negative = negative + 1
        if(averageslope >= 0):
            positive = positive + 1
        else:
            negative = negative + 1
        if(averageslope <= -0.15):
            negative = negative + 2
        if(p <= percentdaymax):
            negative = negative + 1
        else:
            positive = positive + 1
        if(p >= percentyearmax):
            negative = negative + 1
        else:
            positive = positive + 1
        if(p <= percentyearmin):
            negative = negative + 1
        if(eps <= 1.49):
            negative = negative + 1
        else:
            positive = positive + 1
        if(beta > 1):
            negative = negative + 1
        else:
            positive = positive + 1

    return positive, negative, averageslope, totalslope

def main(stockname):
        # Main execution of the sofware
        global globalavg
        error = 0
        try:
                if (stockname == None):
                        stockname = input("what is the symbol of the stock: ")
                stockwebsite = 'https://www.google.com/finance?q=' + stockname
                #stockwebincome = 'https://www.google.com/finance?q=' + stockname + '&fstype=ii'
                while True:
                        stockdata = requests.get(stockwebsite)
                        #stockincomedata = requests.get(stockwebincome)
                        stockdata.raise_for_status()
                        objstockwebsite = bs4.BeautifulSoup(stockdata.text, 'lxml')
                        #objstockincomedata = bs4.BeautifulSoup(stockincomedata.text, 'lxml')
        # searching, sorting and assigning information
                        stocknamedata = objstockwebsite.select('companyName'); #print('test name: ',stocknamedata)
                        stocksummary = objstockwebsite.select('#summary')
                        stockname = stockname.upper()
                        stockpricedata = objstockwebsite.select('div > span > span');
                        stockrangesdata = objstockwebsite.select('.val'); #print(stockrangesdata)
                        #stocknetincomedata = objstockincomedata.select('.hilite .r.bld'); #print('stocknetincome: ',stocknetincomedata)
                        summary = stocksummary[0].getText()
                        #print(summary)
                        stockmarketcap = selectvalue(stockrangesdata[4].getText(), 3)
                        price = selectvalue(stockpricedata[0].getText(),1); #print('Price: ',price)
                        pricehistory.append(price)
                        dayranges = selectvalue(stockrangesdata[0].getText(),2)
                        pricemin = selectvalue(dayranges[1], 1); #print('Daymin: ',pricemin)
                        pricemax = selectvalue(dayranges[0], 1); #print('Daymax: ',pricemax)
                        yearranges = selectvalue(stockrangesdata[1].getText(),2)
                        yearmin = selectvalue(yearranges[1], 1); #print('Yearmin: ', yearmin)
                        yearmax = selectvalue(yearranges[0], 1); #print('Yearmax: ', yearmax)
                        openprice = selectvalue(stockrangesdata[2].getText(), 1); #print('Openprice: ',openprice)
                        PE = selectvalue(stockrangesdata[5].getText(),1); #print('PE: ', PE)
                        div = selectvalue(stockrangesdata[6].getText(), 0)
                        divident = div; #print('Divident: ', divident)
                        eps = selectvalue(stockrangesdata[7].getText(),0); #print('Eps: ',eps)
                        beta = selectvalue(stockrangesdata[9].getText(),1); #print('Beta: ', beta)
                        theoryval = alert(price, pricehistory, openprice, pricemax, pricemin, yearmin, yearmax, eps, beta)
                        pricetarget = pricefinder(globalavg, pricemax, pricemin, openprice)
                        priceyeartarget = pricefinder(globalavg, yearmax, yearmin, openprice)
                        priceoutput = '\n' + stockname + ' ' + '\n Price: ' + price + '\n Openprice: ' + openprice + '\n Daymin: '+ pricemin + ' Daymax: ' + pricemax + ' Yearmin: ' + yearmin + ' Yearmax: ' + yearmax
                        valueoutput = ' PE: ' + PE + ' Divident: ' + divident + ' Eps: ' + eps + ' Beta: ' + beta
                        print(priceoutput); print(valueoutput);print(' Daytarget: ', pricetarget); print(' Yeartarget: ', priceyeartarget); print(' Marketcap: ',stockmarketcap); print(theoryval); error = theory(theoryval)
                        time.sleep(15)       
        except KeyboardInterrupt:
                print('\n Software execution terminated by the user!')

# execution started
main(None)






