// ConsoleApplication1.cpp : Author: Daniel Mejia
//

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

// Global variables to allow the calculation of % by the function comparison
string student[101];
string test[101];

// Comparison value, calculate true or false
int comp(string a, string b)
{

	if (a == b) {
		return true;
	}
	else {
		return false;
	}
}

// loads test asnwer file to a var to be compare with students asnwers
int readf() {
	string Answers;
	int p, size, c;
	int t[1];

	ifstream filein, sfilein;
	sfilein.open("answer.txt");
	sfilein >> t[0];
	size = t[0];
	cout << "\n size = " << size << "\n";
	c = 1;
	for (p = 1; p <= size; p = p + 1)
	{
		do
		{
			sfilein >> Answers;
			test[c] = Answers;
			cout << " " << c << ") ";
			cout << Answers;
			c++;
		} while (Answers != ":" && c < 101);

	}
	cin.get();
	cout << endl;
	sfilein.close();

	return 0;
}

// Read Students Answer file and calculates %
int sreadf() {
	string Answers;
	double percent, correct, wrong, total;
	int p, size, c;
	int t[1];

	ifstream filein, sfilein;
	sfilein.open("Sanswer.txt");
	sfilein >> t[0];
	size = t[0];
	cout << "\n size = " << size;
	for (p = 1; p <= size; p = p + 1)
	{
		c = 1;
		wrong = 0;
		correct = 0;
		sfilein >> Answers;
		cout << "\n \n Student: " << Answers;
		do
		{
			if (Answers != ":") {
				sfilein >> Answers;
				student[c] = Answers;
				
				//cout << Answers;
				if (comp(student[c], test[c]) == 1) {
					//cout << " comp: " << comp(student[c], test[c]);
					//cout << " student[c]: " << student[c];
					//cout << " test[c]: " << test[c];
					correct++;
				}
				else {
					//cout << " comp: " << comp(student[c], test[c]);
					cout << "\n" << c << ") ";
					cout << setw(10) << left << " student: "  << student[c];
					cout << setw(20) << right << "test: " << test[c];
					wrong++;
				}
				c++;
			}
		} while (Answers != ":" && c < 101);
		total = (correct - 1) + wrong;
		percent = (correct - 1) / (total) * 100;
		cout << "\n" << correct - 1 << "/" << total << " Student%: " << percent;
		
	}
	cout << endl;
	sfilein.close();

	return 0;
}

// Write Students answer file
int writeSF() {
	int p, count, count2, st;
	char questions[101];
	char Sname[101];

	ofstream fileout;
	fileout.open("Sanswer.txt");
	cout << "How many students: ";
	cin >> p;
	cin.ignore();
	fileout << p << endl;
	if (p > 100 || p < 1) {
		cout << "Value out of range!" << endl;
	}
	else {
		count = 0;
		
		
		cout << "What are the solutions: \n";
		for (count = 0; p > count; count = count + 1)
		{
			cout << "Name of Student: ";
			cin.getline(Sname, 101);
			fileout << Sname << " ";
			cout << "Answers " << ": ";
			cin.getline(questions, 101);
			fileout << questions  << " :" << endl;
		}
		fileout.close();
	}
	return 0;

} 

// Add Student to current file
int writeA() {
	int p, count, count2, st;
	char questions[101];
	char Sname[101];

	ofstream fileout;
	fileout.open("Sanswer.txt", ios::app);
	cout << "How many students: ";
	cin >> p;
	cin.ignore();
	fileout << p << endl;
	if (p > 100 || p < 1) {
		cout << "Value out of range!" << endl;
	}
	else {
		count = 0;


		cout << "What are the solutions: \n";
		for (count = 0; p > count; count = count + 1)
		{
			cout << "Name of Student: ";
			cin.getline(Sname, 101);
			fileout << Sname << " ";
			cout << "Answers " << ": ";
			cin.getline(questions, 101);
			fileout << questions << " :" << endl;
		}
		fileout.close();
	}
	return 0;

}

// Write answer file to be compare with the Students file for % of correctness
int writef() {
	int p, count;
	char questions[101];

	ofstream fileout;
	fileout.open("Answer.txt");
	cout << "How many variables to compare? \n" << endl;
	cout << "Enter: ";
	cin >> p;
	cin.ignore();
	fileout << p << endl;
	if (p > 100 || p < 1) {
		cout << "Value out of range!" << endl;
	}
	else {
		count = 0;
		cout << "What are the solutions: \n";
		for (count = 0; p > count; count = count + 1)
		{
			cout << "Question " << count + 1 << ": ";
			cin.getline(questions, 101);
			fileout << questions << " :" << endl;
		}
		fileout.close();
	}
	return 0;
}


int main(int choice)
{
	int choice;
do {
	cout << "\n 1) Write new answer file" << "\n 2) Write new student file" << "\n 3) Add student to current file" << "\n 4) Check results" << "\n 5) Close app";
	cout << "\n What routine to execute: ";
	cin >> choice;
	switch (choice) {
	case 1:
		writef();
		break;
	case 2:
		writeSF();
		break;
	case 3:
		writeA();
		break;
	case 4:
		readf();
		sreadf();
		break;
	}
} while (choice != 5);
	return 0;
}