/*-----------------------------------------------------------------------------------*
 * Author: Daniel Mejia                                                              *
 *                                                                                   *
 * Purpose: The program purpose is to help with basic DC electronic courses          *
 *          calculation and some advance classes such as AC electronics.             *                                                                              *
 *                                                                                   *
 * Copyright: Modified only for the purpose of learning don't copy the code to       *
 *            implement to another software for selling purposes.                    *
 *                                                                                   *
 *                                                                                   *
 * ----------------------------------------------------------------------------------*/


#include <stdio.h>
#include <math.h>

	double matrix [2][2];
	double matrix2 [2][2];
	double matrix3 [2][2];
	double  a = 0, b = 0, d = 0, a2 = 0, b2 = 0, d2 = 0;
	double x = 0,y = 0;
	double matrix_answer, matrix2_answer, matrix3_answer;
    void displaymatrix(double matrix[2][2]); 
     
void current_killer (double test)
{
	if (test > 0.1 && test <= 0.2) {
			printf ("Deadly current hearth defibrillation, paintful\n");
		}
	else if (test > 0.2) {
			printf ("Good chance of survival but severe burn.\n");
		}
	else if (test <= 0.1 && test > 0.01) {
			printf ("Not deadly but painful\n");
		}
	else {
		printf ("undetectable, safe current.\n");
	}
}
	
void calculate_matrix (void)
{
	
	// first matrix variable value elc11 implementation

matrix [1][1] = a;
matrix [1][2] = b;
matrix [2][1] = a2;
matrix [2][2] = b2;

// Second matrix variable elc11 implementation

matrix2 [1][1] = d;
matrix2 [1][2] = b;
matrix2 [2][1] = d2;
matrix2 [2][2] = b2;

// Third matrix variable elc11 implementation

matrix3 [1][1] = a;
matrix3 [1][2] = d;
matrix3 [2][1] = a2;
matrix3 [2][2] = d2;

	printf ("a = %lf b = %lf d = %lf a2 = %lf b2 = %lf d2 = %lf\n", a, b, d, a2, b2, d2);
	
	 matrix_answer = a * b2 - a2 * b;
     matrix2_answer = d * b2 - d2 * b;
     matrix3_answer = a * d2 - a2 * d;
	
	printf ("\nFirst matrix\n");
	displaymatrix (matrix);
	printf ("Determinant = %lf", matrix_answer);
	printf("\n");
	printf ("\nSecond matrix\n");
	displaymatrix (matrix2);
	printf ("Determinant = %lf", matrix2_answer);
	printf("\n");
	printf ("\nThird matrix\n");
	displaymatrix (matrix3);
	printf ("Determinant = %lf", matrix3_answer);
	printf("\n");
   
   x = matrix2_answer / matrix_answer;
   y = matrix3_answer / matrix_answer;
   printf ("\nX = %1.2lf and Y = %1.2lf\n", x, y);
      
 }
	
void displaymatrix (double matrix[2][2])
	{
		int row, column;
	matrix [0][0] = 0;
	matrix [0][1] = 0;
		
		for (row = 1; row < 3; ++row) 
		{
			for (column = 1; column < 3; ++column)
			printf ("%1.1lf ", matrix[row][column]);
		    printf ("\n");
		}
	}
void usage (char *name)
{
	void exit(int __status);
	printf ("Usage %s selection #\n", name);
	printf ("selection\n");
	printf ("----------------\n");
	printf (" 1 DC-Analysis\n");
	printf (" 2 AC-Analysis\n");
	printf (" 3 Waves-Distance travel\n");
    printf (" 4 Mesh analysis\n\n");
	exit (1);
}

int main (int argc, char *argv[]) 
{
	int atoi(const char *__nptr);
	void usage (char *name);
	void calculate_matrix (void);
	int command, loop;            
	int frequency, amount;   
	const float sound = 344.4;                   //sound speed meter/second!
	const float magneticwave = 3 * pow(10,8);
	char command2, selection;                   // Switch #1 DC
	char selection2, selection3;                // Switch #2 AC and #3 Waves
	double answer, parallel[20], tresistance;
	void displaymatrix (double matrix[2][2]);
	void current_killer(double test);
	double c[20]; 
	double i, v, r;                            // i = currrent, v = voltage, r = resistance
	
    if (argc <= 1) {
      usage(argv[0]);
    }
    
    command = atoi(argv[1]);
    
  switch(command)
  {
	 case 1:
	  printf ("Circuit is p parallel or s series?\n");
	  printf ("selection = ");
	  scanf ("%c", &command2);
	 
	   if (command2 == 'p')
	   {
	    printf ("what is the total voltage? ");
		scanf ("%lf", &v);
		printf ("how many parallel resistors are in the circuit ?: ");
        scanf ("%i", &amount);       
        
       if (amount > 20 || amount < 1)
    {
           printf ("Either to many values or negative values are being use:\n");
           return 2;
     }
      else
           printf ("what are the values of the resistors?: \n");    
    for (loop = 1; loop <= amount; loop++)
    {
       	   scanf ("%lf", &parallel[loop]);
       	   answer += 1 / parallel[loop];
       	   c[loop] = v / parallel[loop];
	}
	
	for (loop = 1; loop <= amount; loop++) {
       	   printf ("current for resistor%i is %lf A\n", loop, c[loop]);
     }
       
           tresistance = 1 / answer;
           
           printf ("\nthe total resistance is %3.3lf Ohm\n", tresistance);   
           printf ("the  conductance is %lf Siemen\n", answer);  
           break;
	   }
              
	   if (command2 == 's')
	       {
	           printf ("what to find in the ohm law\n");
               printf ("current = c ");
               printf ("voltage = v ");
               printf ("resistance = r \n");
               printf ("Please enter value from the above: ");
               scanf ("%c", &selection);
               scanf ("%c", &selection);                        //Scanf doesn't work without the second scanf function
		   }
                    
     switch(selection)
              {

  // case 1 analyse current
              case 'c':
              printf ("\n----------------------------------\n");
              printf ("what is the total voltage: ");
              scanf ("%lf", &v);
		      printf ("what is the total resistance: ");
              scanf ("%lf", &r);
              i = v / r;
              printf ("the current is equal to %4.2lf A\n\n", i);
              current_killer(i);
              break;

// case 2 analyse voltage
               case 'v':
               printf ("what it the total resistance: ");
               scanf ("%lf", &r);
               printf ("what is the total current: ");
               scanf ("%lf", &i);
               v = r / i;
               printf ("the voltage is equal to %4.2lf V\n", v);
               break;

// case 3 analyse resistance
               case 'r':
               printf ("\n----------------------------------\n");
               printf ("what is the total voltage: ");
               scanf ("%lf", &v);
               printf ("what is the total current: ");
               scanf ("%lf", &i);
               r = v / i;
               printf ("the resistance is = %4.2lf Ohm\n", r);
               break;
}
  	break;
       case 2:
        printf("Under construction we apologize, it will be done soon\n");
        break;
       
       case 3:
        printf ("what wave distance to find\n");
        printf (" s Sound wave\n");
        printf (" e Electromanetic Wave\n");
        printf("\nSelection = ");
             
        scanf ("%c", &selection3);
       
       switch(selection3)
       {
          case 's':
           printf ("what is the frequency(Hertz)\n");
           scanf ("%i", &frequency);
           answer = sound / frequency;
           printf ("\nThe distance is 3%.3lf Meters/second\n", answer);
           break;
           
          case 'e':
           printf ("what is the frequency(Hertz)\n");
           scanf ("%i", &frequency);
           answer = magneticwave / frequency;
           printf ("\nThe soluction is %3.3lf Meters/second\n", answer);
           break;
	   }
 case 4:
	printf ("First equation\n");
	printf ("what is the first value: ");
	scanf ("%lf", &a);
	printf ("what is the second value: ");
	scanf ("%lf", &b);
	printf ("what is the third value: ");
	scanf ("%lf", &d);
	printf ("\nSecond equation\n");
	printf ("what is the first value: ");
	scanf ("%lf", &a2);
	printf ("what is the second value: ");
	scanf ("%lf", &b2);
	printf ("what is the third value: ");
	scanf ("%lf", &d2);
	calculate_matrix();
	break;
   }
   return 0;
}
